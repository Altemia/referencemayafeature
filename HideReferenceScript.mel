//------------------------------------------------------------------
/// Reference Implementation.
//------------------------------------------------------------------
{
//------------------------------------------------------------------
/// The name of the File Reference Data is stored in.
/// - Should not be called externally.
//------------------------------------------------------------------
global string $tempDirectory;

//------------------------------------------------------------------
/// Initialise
//------------------------------------------------------------------
global proc Initialise()
{
	$tempDirectory = `internalVar -userTmpDir`;
	$tempDirectory += "daveMayaReferencing/";
}

//------------------------------------------------------------------
/// Load all names of all reference objects.
//------------------------------------------------------------------
global proc string[] LoadAllReferenceData(string $referencesFileName)
{
	string $results[];
	string $loadFilePath = $tempDirectory + $referencesFileName;

	int $fileTestResult = `filetest -r $loadFilePath`;
	if($fileTestResult == 1)
	{
	    string $referenceIdentifiers[] = freadAllLines($loadFilePath);
	    
	    for($line in $referenceIdentifiers)
	    {    	
	    	if(size($line) > 0)
	    	{
		    	print("HideReferenceScript:LoadReferenceData - A Line - " + $line + "\n");

		    	string $referenceIdentifier = GetReferenceIdentifierScript($line);
		    	string $referenceFilePath = $tempDirectory + $referenceIdentifier + ".txt";

				string $referenceContents[] = LoadReferenceObjectIdentifiers($referenceFilePath);
				appendStringArray($results, $referenceContents, size($referenceContents));

			    //-- Delete namespace
			    catch(`namespace -deleteNamespaceContent -removeNamespace $referenceIdentifier`);
			}
	    }
	}

	return $results;
}

//------------------------------------------------------------------
global proc string[] LoadReferenceObjectIdentifiers(string $in_referenceFilePath)
{
	string $referenceContents[];

	$fileTestResult = `filetest -r $in_referenceFilePath`;
	if($fileTestResult == 1)
	{
		$referenceContents = freadAllLines($in_referenceFilePath);
	}
	else
	{
		print("HideReferenceScript:LoadReferenceObjectIdentifiers - " + $in_referenceFilePath + " did not exist \n");
	}

	return $referenceContents;
}

//------------------------------------------------------------------
/// Clear Scene of Unwanted content
//------------------------------------------------------------------
global proc ClearReferences(string $in_toDelete[])
{
	for($line in $in_toDelete)
	{
		if(size($line) > 0)
		{
			print("HideReferenceScript:ClearReferences - Reference Object - " + $line + "\n");

			catch(`lockNode -lockName false $line`);

			if(catch(`delete $line`))
			{
			    print("Couldn't find object: " + $line);
			}
		}
	}
}

//------------------------------------------------------------------
/// Main method called externally.
//------------------------------------------------------------------
global proc HideReferenceScript()
{
    Initialise();
	string $referenceDataFileName = ReferenceConfigScript($tempDirectory);

    string $readFileContents[] = LoadAllReferenceData($referenceDataFileName);
    ClearReferences($readFileContents);
}
}